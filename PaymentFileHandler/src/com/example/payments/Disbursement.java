package com.example.payments;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.regex.Pattern;


/**
 * 
 * @author Björn Dalberg
 * Class for processing a disbusement file
 *
 */

public class Disbursement {

	private static final Charset CHARSET = Charset.forName("ISO-8859-1");
	private Path incorrectFilesDir = Paths.get("/Users/bjorn/Desktop/Transactions/incorrect_files");
	private Transaction transaction;

	public Disbursement() {
	}

	public Transaction transactionReader(Path path) {
		BufferedReader reader = null;
		ArrayList<TransactionPost> paymentPosts = new ArrayList<>();
		System.out.println("Disbursement file received.");
		if (Files.exists(path, NOFOLLOW_LINKS)) {
			try {
				// Depending on the file size Files.readAllLines might be an
				// easier
				// implementation as shown in the Deposit class
				reader = Files.newBufferedReader(path, CHARSET);
				String startPost = reader.readLine();

				// Check if start post has the correct format
				if (startPost.startsWith("O") && startPost.length() == 51) {
					String paymentFileLines = null;
					while ((paymentFileLines = reader.readLine()) != null) {
						// Create new Transaction object to store values from
						// file
						transaction = new Transaction();
						String accountNumber = startPost.substring(1, 16).trim();

						// Test that the account no. only contains digits and a
						// single space
						Pattern pattern = Pattern.compile("[0-9]+\\s[0-9]+");
						if (pattern.matcher(accountNumber).matches()) {
							transaction.setAccountNumber(accountNumber);
						} else
							throw new IllegalArgumentException("Incorrect account number");

						// Create a BigDecimal and replace the comma sign with a
						// dot. Set the number of decimals to 2
						BigDecimal totalAmount = new BigDecimal(startPost.substring(16, 30).trim().replace(",", "."));
						totalAmount = totalAmount.setScale(2);
						transaction.setTotalAmount(totalAmount);

						transaction.setTransactionDate(
								new SimpleDateFormat("yyyyMMdd").parse(startPost.substring(40, 48)));

						transaction.setCurrancy(startPost.substring(48, startPost.length()));

						// Store all payment posts in ArrayList before adding
						// them to the Transaction object
						if (paymentFileLines.startsWith("B")) {
							BigDecimal amount = new BigDecimal(
									paymentFileLines.substring(1, 15).trim().replace(",", "."));
							amount = amount.setScale(2);
							TransactionPost transactionPost = new TransactionPost(amount,
									paymentFileLines.substring(15, paymentFileLines.length()));
							paymentPosts.add(transactionPost);
						} else {
							throw new IllegalArgumentException("Payment post format is incorrect.");
						}
					}
				} else {
					throw new IllegalArgumentException("Start post has an incorrect format");
					
				}
				transaction.setTransactionPost(paymentPosts);

				// Verify that the total amount in the start post and
				// the amounts from each payment adds up
				BigDecimal sum = new BigDecimal("0");
				sum = sum.setScale(2);
				for (TransactionPost transactionPost : paymentPosts)
					sum = sum.add(transactionPost.getAmount());
				if (sum.compareTo(transaction.getTotalAmount()) != 0) {
					System.out.println("Sum from deposit posts vs total amount from end post are not equal. Calculated sum: " + sum + " TotalAmount from file: "
							+ transaction.getTotalAmount());
					throw new IllegalArgumentException("Transaction posts amounts does not equal start post total amount");	
				}
				reader.close();
			} catch (IOException | IllegalArgumentException | ParseException e) {
				transaction = null;
				e.printStackTrace();
				try {
					reader.close();
					Files.move(path, incorrectFilesDir.resolve(path.getFileName()), REPLACE_EXISTING);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		return transaction;
	}
}
