package com.example.payments;

import java.math.BigDecimal;

/**
 * Stores the amount and reference for each post in a transaction
 * @author Björn Dalberg
 * @param amount The amount from the given post
 * @param reference The reference to the given post
 */

public class TransactionPost {
	private BigDecimal amount;
	private String reference;
	
	public TransactionPost(BigDecimal amount, String reference) {
		setAmount(amount);
		setReference(reference);
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
}
