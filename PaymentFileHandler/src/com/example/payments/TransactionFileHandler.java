package com.example.payments;

import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.IOException;

/**
 * 
 * @author Björn Dalberg
 * Handler object for transaction files. First checks that 
 * the file name is valid and instatiates the appropriate object.
 * If a file process is successfull the payment receicer is called.
 *
 */

public class TransactionFileHandler implements PaymentReceiver {

	private Path unknownFilesDir = Paths.get("/Users/bjorn/Desktop/Transactions/unknown_files");
	private Path completedTransactions = Paths.get("/Users/bjorn/Desktop/Transactions/completed_transactions");
	private Transaction transaction;

	public TransactionFileHandler(Path path) {
		Path filename = path.getFileName();
		System.out.println(filename);
		try {
			if (filename.toString().endsWith("_betalningsservice.txt")) {
				Disbursement disbursement = new Disbursement();
				transaction = disbursement.transactionReader(path);
			} else if (filename.toString().endsWith("_inbetalningstjansten.txt")) {
				Deposit deposit = new Deposit();
				transaction = deposit.transactionReader(path);
			} else {
				System.out.println("Unknown file " + filename + "\nMoving to unknown_files directory.");
				Files.move(path, unknownFilesDir.resolve(filename), REPLACE_EXISTING);
			}
			
			//If processing of transaction failed, it will be null
			if (transaction != null) {
				//Start payment bundle, get data from transaction object
				startPaymentBundle(transaction.getAccountNumber(), transaction.getTransactionDate(),
						transaction.getCurrancy());
				
				//transmit all posts in the transaction
				for (TransactionPost transactionPost : transaction.getTransactionPost()) {
					payment(transactionPost.getAmount(), transactionPost.getReference());
				}
				endPaymentBundle();
				
				//Move processed file to completed files directory
				Files.move(path, completedTransactions.resolve(filename), REPLACE_EXISTING);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void startPaymentBundle(String accountNumber, Date paymentDate, String currency) {
		// call to open receiver. Should be a call to an unknown service
		System.out.println("Payment bundle started...");
		System.out.println("AccountNumber: " + accountNumber);
		System.out.println("Date: " + paymentDate);
		System.out.println("Currency: " + currency);

	}

	@Override
	public void payment(BigDecimal amount, String reference) {
		// send payment to reciever
		System.out.println("Amount: " + amount);
		System.out.println("Reference: " + reference);
	}

	@Override
	public void endPaymentBundle() {
		// call to close receiver
		System.out.println("Payment bundle closed...");
	}
}
