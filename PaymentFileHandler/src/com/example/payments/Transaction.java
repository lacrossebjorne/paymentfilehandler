package com.example.payments;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/**
 * 
 * @author Björn Dalberg
 * DTO for a single transaction. Stored the relevant data from a payment file.
 *
 */

public class Transaction {
	private String accountNumber, currancy, reference;
	private int entries;
	private Date transactionDate;
	private BigDecimal totalAmount;
	private ArrayList<TransactionPost> transactionPost;
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getCurrancy() {
		return currancy;
	}
	public void setCurrancy(String currancy) {
		this.currancy = currancy;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public int getEntries() {
		return entries;
	}
	public void setEntries(int entries) {
		this.entries = entries;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public ArrayList<TransactionPost> getTransactionPost() {
		return transactionPost;
	}
	public void setTransactionPost(ArrayList<TransactionPost> transactionPost) {
		this.transactionPost = transactionPost;
	}
	 
	
}
