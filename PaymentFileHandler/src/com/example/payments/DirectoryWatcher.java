package com.example.payments;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.IOException;

public class DirectoryWatcher {

	public static void main(String[] args) {
	
		try {
			WatchService watcher = FileSystems.getDefault().newWatchService();
			Path directory = Paths.get("/Users/bjorn/Desktop/Transactions");
			directory.register(watcher, ENTRY_CREATE);
			
			System.out.println(directory.getFileName());
			
			while (true) {
				WatchKey key;
				try {
					key = watcher.take();
				} catch (InterruptedException e) {
					return;
				}
				
				for (WatchEvent<?> event : key.pollEvents()) {
					WatchEvent.Kind<?> kind = event.kind();
					
					if (kind == OVERFLOW)
						continue;
					@SuppressWarnings("unchecked")
					WatchEvent<Path> eventPath = (WatchEvent<Path>) event;
					Path filename = eventPath.context();
					
					new TransactionFileHandler(directory.resolve(filename));
					
					boolean resetKey = key.reset();
					if (!resetKey)
						break;
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}

}
