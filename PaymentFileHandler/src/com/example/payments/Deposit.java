package com.example.payments;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 
 * @author Björn Dalberg
 * Class for processing a deposit file
 *
 */

public class Deposit {

	private static final Charset CHARSET = Charset.forName("ISO-8859-1");
	private Path incorrectFilesDir = Paths.get("/Users/bjorn/Desktop/Transactions/incorrect_files");
	private Transaction transaction;

	public Deposit() {}

	public Transaction transactionReader (Path path) {
		System.out.println("Deposit file received");
		if (Files.exists(path, NOFOLLOW_LINKS)) {
			try {
				// Depending on the file size BufferReader (for example
				// Files.newBufferReader) might be a better option to use.
				// But given the sample data I've decided to implement
				// Files.readAllLines.
				List<String> depositFileLines = Files.readAllLines(path, CHARSET);
				String startPost = depositFileLines.get(0);
				String endPost = depositFileLines.get(depositFileLines.size() - 1);

				// Verify that the start post and end post are correct and that
				// the number of deposits match the entered number in the end
				// post
				if (startPost.startsWith("00") && startPost.length() == 80 && endPost.startsWith("99")
						&& (Integer.parseInt(endPost.substring(30, 38)) == depositFileLines.size() - 2)
						&& endPost.length() == 80) {

					transaction = new Transaction();
					String clearingNumber = startPost.substring(10, 14);
					String accountNumber = startPost.substring(14, 24);

					// Test that clearing number and account number contains
					// digits only
					Pattern pattern = Pattern.compile("[0-9]+");
					if (pattern.matcher(clearingNumber).matches() && pattern.matcher(accountNumber).matches()) {
						transaction.setAccountNumber(clearingNumber + " " + accountNumber);
						;
					} else
						throw new IllegalArgumentException("Incorrect account number");

					// Create a BigDecimal and replace the comma sign with a
					// dot. Set the number of decimals to 2
					BigDecimal totalAmount = new BigDecimal(endPost.substring(2, 22));
					totalAmount = totalAmount.setScale(2);
					transaction.setTotalAmount(totalAmount);

					// Store all deposit posts in ArrayList before adding
					// them to the Transaction object
					ArrayList<TransactionPost> depositPosts = new ArrayList<>();
					for (String line : depositFileLines) {
						if (line.startsWith("30")) {
							BigDecimal amount = new BigDecimal(line.substring(2, 22));
							amount = amount.setScale(2);
							String reference = line.substring(40, 65);
							TransactionPost deposit = new TransactionPost(amount, reference);
							depositPosts.add(deposit);
						}
					}

					// Verify that the total amount in the start post and
					// the amounts from each deposit adds up
					BigDecimal sum = new BigDecimal("0");
					sum = sum.setScale(2);
					for (TransactionPost deposit : depositPosts)
						sum = sum.add(deposit.getAmount());
					if (sum.compareTo(transaction.getTotalAmount()) != 0) {
						System.out.println("Sum from deposit posts vs total amount from end post are not equal. Calculated sum: " + sum + " TotalAmount from file: "
								+ transaction.getTotalAmount());
						throw new IllegalArgumentException("Transaction posts amounts does not equal start post total amount");
					}
					transaction.setTransactionPost(depositPosts);
					transaction.setTransactionDate(new Date());
					// Assuming the currancy is SEK
					transaction.setCurrancy("SEK");

				} else
					throw new IllegalArgumentException("Deposit file is not formatted correctly");
			} catch (IOException | IllegalArgumentException e) {
				transaction = null;
				e.printStackTrace();
				try {
					Files.move(path, incorrectFilesDir.resolve(path.getFileName()), REPLACE_EXISTING);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		return transaction;
	}
}
